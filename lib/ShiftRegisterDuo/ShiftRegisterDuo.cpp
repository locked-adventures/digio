#include "ShiftRegisterDuo.hpp"

#include <Arduino.h>
#include <SPI.h>

void ShiftRegisterDuo::init() {
    pinMode(hc165_shift, OUTPUT);

    digitalWrite(hc595_clear, HIGH);
    pinMode(hc595_clear, OUTPUT);
    
    digitalWrite(hc595_storageClock, HIGH);
    pinMode(hc595_storageClock, OUTPUT);

    digitalWrite(hc595_outputEnable, HIGH);
    pinMode(hc595_outputEnable, OUTPUT);

    SPI.begin();
}

void ShiftRegisterDuo::clearOutput() {
    // Arduino clock frequency is f = 16 MHz, T = 62.5ns
    // It is slow enough so that when writing to pins no extra delays are necessary.
    digitalWrite(hc595_clear, LOW);
    digitalWrite(hc595_clear, HIGH);
}

void ShiftRegisterDuo::setOutputEnabled(bool enabled) {
    digitalWrite(hc595_outputEnable, enabled ? LOW : HIGH);
}

void ShiftRegisterDuo::transfer(bool in[], const bool out[]) {
    int nBytes = (nPins + 7) / 8;

    uint8_t arr[nBytes] = {};

    if (out) {
        for (int i = 0; i < nPins; i++) {
            arr[nBytes - 1 - i / 8] |= int(out[i]) << (i % 8);
        }
    }

    SPI.beginTransaction(SPISettings(spiSpeed, MSBFIRST, SPI_MODE0));
    digitalWrite(hc165_shift, HIGH);  // Start shifting HC165
    digitalWrite(hc595_storageClock, LOW);
    SPI.transfer(arr, nBytes);
    digitalWrite(hc165_shift, LOW);  // Stop shifting HC165 and start loading again
    digitalWrite(hc595_storageClock, HIGH);  // HC595: write shift register state to storage register
    SPI.endTransaction();

    if (in) {
        for (int i = 0; i < nPins; i++) {
            in[i] = (arr[nBytes - 1 - i / 8] >> (i % 8)) & 1;
        }
    }
}
