#include <ArduinoJson.h>
#include <SPI.h>

#include <ShiftRegisterDuo.hpp>

#define SERIAL_SPEED 115200

#define SPI_SPEED 1000000

#define IN  "in"
#define OUT "out"
#define INIT "init"

#define HC595_CLEAR         (2)
#define HC595_STORAGE_CLOCK (3)
#define HC595_OUTPUT_ENABLE (4)
#define HC165_SHIFT         (5)

#define N_PINS (16)

#define LOOP_DELAY 5
#define INPUT_STABLE_CYCLES 5

#define ACK "ack"
#define REQUEST_ID "reqid"

#define ERROR "error"
#define TYPE "type"
#define CODE "code"
#define DESCRIPTION "description"

#define JSON_DOC_SIZE 300

bool out[N_PINS];
bool in[N_PINS];

int inCounter[N_PINS];

/*
inline StaticJsonDocument<JSON_DOC_SIZE> createJsonDoc() {
  return StaticJsonDocument<JSON_DOC_SIZE>();
}
*/

/*
inline DynamicJsonDocument createJsonDoc() {
  return DynamicJsonDocument(JSON_DOC_SIZE);
}
*/

ShiftRegisterDuo shiftRegisterDuo(
  HC165_SHIFT,
  HC595_CLEAR,
  HC595_STORAGE_CLOCK,
  HC595_OUTPUT_ENABLE,
  SPI_SPEED,
  N_PINS);

template <typename T>
static void arrayToJson(const T array[], size_t size, JsonVariant json) {
  json.to<JsonArray>();
  for (size_t i = 0; i < size; i++) {
    if (!json.add((uint8_t) array[i])) {
      Serial.println("{\"error\": \"JSON doc too small!\"}");
    }
  }
}

template <typename T>
int arrayMerge(T dest[], size_t size, JsonVariantConst src) {
  JsonObjectConst obj = src.as<JsonObjectConst>();
  if (obj) {
    for (JsonPairConst kvp : obj)
    {
      unsigned index = atoi(kvp.key().c_str());
      if (index < size) {
        dest[index] = kvp.value().as<T>();
      }
    }
    return 0;
  }
  
  JsonArrayConst arr = src.as<JsonArrayConst>();
  if (arr) {
    for (size_t i = 0; i < arr.size() && i < size; i++) {
      dest[i] = arr[i].as<T>();
    }
    return 0;
  }

  // Other types not supported
  return -1;
}

constexpr size_t LINE_SIZE = 64;
char line[LINE_SIZE];
size_t line_len = 0;

bool inNew[N_PINS];

StaticJsonDocument<JSON_DOC_SIZE> clientJson;
StaticJsonDocument<JSON_DOC_SIZE> outputJson;

void setup() {
  Serial.begin(SERIAL_SPEED);
  while (!Serial);
  shiftRegisterDuo.init();
  shiftRegisterDuo.transfer(in, out);
  shiftRegisterDuo.setOutputEnabled(true);

  outputJson[INIT] = true;
  outputJson[IN].to<JsonArray>();
  arrayToJson(in, N_PINS, outputJson[IN]);
  outputJson[OUT].to<JsonArray>();
  arrayToJson(out, N_PINS, outputJson[OUT]);

  Serial.println("\r\nBEGIN");

  serializeJson(outputJson, Serial);
  Serial.println();
  outputJson.clear();
}

// Handle client input line
void handleLine(const char line[]) {
  outputJson.clear();
  auto &ackJson = outputJson;
  DeserializationError err = deserializeJson(clientJson, line);
  ackJson[ACK].to<JsonObject>();
  if (err == DeserializationError::Ok) {
    JsonObject obj = clientJson.as<JsonObject>();
    if (obj) {
      ackJson[ACK][REQUEST_ID] = clientJson[REQUEST_ID];
      JsonVariant clientOutput = clientJson[OUT];
      if (!clientOutput.isNull()) {
        if (arrayMerge(out, N_PINS, clientJson[OUT])) {
          ackJson[ACK][ERROR][TYPE] = "content";
          ackJson[ACK][ERROR][DESCRIPTION] = "invalid output request";
        }
        else {
          ackJson[ACK][OUT].to<JsonArray>();
          for (auto o : out) {
            if (!ackJson[ACK][OUT].add((uint8_t) o)) {
              Serial.println("{\"error\": \"JSON doc too small!\"}");
            }
          }
          // Print output array for debugging
          /* Serial.print("out: [");
          bool first = true;
          for (auto o : out) {
            if (!first)
              Serial.print(", ");
            Serial.print(o);
            first = false;
          }
          Serial.println("]"); */
        }
      }
    }
    else {
      ackJson[ACK][ERROR][TYPE] = "content";
      ackJson[ACK][ERROR][DESCRIPTION] = "request should be an object";
    }
  }
  else {
    ackJson[ACK][ERROR][TYPE] = "deserialization";
    ackJson[ACK][ERROR][CODE] = err.c_str();
  }
  // ackJson["testkey"] = "testval";
  serializeJson(ackJson, Serial);
  Serial.println();
  ackJson.clear();
}

bool line_overflow = false;

void loop() {
  if (!line_overflow) {
    int c = Serial.read();
    if (c != -1) {
      char ch = c;
      if (ch == '\0') {
        Serial.println("{\"error\": \"Input char cannot be \'\\0\'!\"}");
      }
      else {
        line[line_len] = ch;
        line_len++;
        line[line_len] = '\0';
        if (ch == '\n') {
          handleLine(line);
          line_len = 0;
          line[line_len] = '\0';
        }
        else if (line_len >= LINE_SIZE - 1) {
          Serial.println("{\"error\": \"Line is becoming too large!\"}");
          line_overflow = true;
        }
      }
    }
  }

  shiftRegisterDuo.transfer(inNew, out);

  auto &eventJson = outputJson;
  eventJson.clear();
  for (size_t i = 0; i < N_PINS; i++) {
    if (inNew[i] == in[i]) {
      // Reset de-bouncing counter
      inCounter[i] = 0;
    }
    else {
      // De-bouncing
      inCounter[i]++;
      if (inCounter[i] >= INPUT_STABLE_CYCLES) {
        in[i] = inNew[i];

        // eventJson[IN].to<JsonObject>();

        char key[4];
        // No official way to print size_t, so we have to convert to long
        if (snprintf(key, sizeof(key), "%lu", (unsigned long) i) < 0) {
          Serial.println("{\"error\": \"snprintf failed!\"}");
        }
        eventJson[IN][key] = (uint8_t) in[i];

        // Reset de-bouncing counter
        inCounter[i] = 0;
      }
    }
  }

  if (!eventJson.isNull()) {
    serializeJson(eventJson, Serial);
    Serial.println();
  }

  eventJson.clear();

  Serial.flush();

  delay(LOOP_DELAY);
}
